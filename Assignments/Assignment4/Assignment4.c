#include <stdlib.h>
#include <stdio.h>

#include "pgm.h"

#define BINARY 1
#define RGB 2
#define TEXT 3

int main(int argc, char **argv)
{
    int status;
    int fileType;

    pgmStruct pgmImage;
    pgmImage.pixels = NULL;
    pgmImage.pixelsB = NULL;
    pgmImage.pixelsI = NULL;

    if(argc < 3)
    {
        printf("please include an input file name and an output file name");
        return 1;
    }

    char* infilename = argv[1];
    printf("input file name: %s \n", infilename);
    char* outfilename = argv[2];
    printf("output file name: %s \n", outfilename);

    //1. Open	the	input	file
    FILE *pgmFile = NULL;
    pgmFile = fopen(infilename, "r");

    if (pgmFile == NULL)
    {
        printf("Error opening %s \n", infilename);
        return 2;
    }

    //2. Read	the	tag	to	determine the	format
    char tag[3];
    printf("READ FILE TYPE... ");
    status = fscanf(pgmFile, "%s", tag);
    printf("status: %d, ", status);
    printf("tag: '%s' \n", tag);

    if (strcmp(tag, "P2") == 0)
    {
        fileType = TEXT;
    }
    else if (strcmp(tag, "P5") == 0)
    {
        fileType = BINARY;
    }
    else if (strcmp(tag, "P6") == 0)
    {
        fileType = RGB;
    }
    else
    {
        printf("Unknown file type");
        return 3;
    }

    //3. Skip	the	comments
    // Todo: fixme
    printf("Skip comments... \n");
    char buffer[256];
    fgets(buffer, 256, pgmFile); // Skip rest of line
    fgets(buffer, 256, pgmFile);
    if (fileType == TEXT)
    {
        fgets(buffer, 256, pgmFile);
    }


    // Read width/height
    printf("Read dimensions... ");
    int w, h;
    fscanf(pgmFile, "%d", &w);
    printf("width: %d, ", w);
    fscanf(pgmFile, "%d", &h);
    printf("height: %d\n", h);

    // Read colordepth
    printf("Read depth... ");
    int d;
    fscanf(pgmFile, "%d", &d);
    printf("depth: %d\n", d);

    pgmImage.width = w;
    pgmImage.height = h;
    pgmImage.intensity = d;

    //    Correctly	read	the	P2	image	and	save	with	the	P5	image	format	(30	points)
    //    Correctly	read	the	P5	image	and	save	with	the	P5	image	format	(30	points)
    //    Correctly	read	the	P6	image	and	save	with	the	P6	image	format	(30	points)
    if (fileType == TEXT)
    {
        ProcessP2(pgmFile, &pgmImage);
    }
    else if(fileType == BINARY)
    {
        ProcessP5(pgmFile, &pgmImage);
    }
    else // binary rgb
    {
        ProcessP6(pgmFile, &pgmImage);
    }

    //5. Open	the	output	file
    printf("\nOPEN OUTPUT FILE...");
    FILE *output = NULL;
    output = fopen("output.bin", "wb");

    if (output == NULL)
    {
        printf("Error opening output.bin\n");
        return 3;
    }


    //7. Release	the	memory
    fclose(pgmFile);
    fclose(output);

    // free allocated memory
    if (pgmImage.pixels != NULL){
        free(pgmImage.pixels);
    }

    if (pgmImage.pixelsB != NULL){
        free(pgmImage.pixelsB);
    }

    if (pgmImage.pixelsI != NULL){
        free(pgmImage.pixelsI);
    }

    return 0;
}
