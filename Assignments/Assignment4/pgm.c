#include "pgm.h"

void ProcessP2(FILE * pgmFile, pgmStruct * ptrPgm)
{
    int status;

    //4. Dynamically	allocate	the	memory	and	Read	the	data	into	a	buffer
    printf("Allocate memory... ");

    int memSize = ptrPgm->height * ptrPgm->width * sizeof(Pixel);

    printf("%d bytes, ", memSize);

    ptrPgm->pixelsI = malloc(memSize);

    int pixelIndex = 0;
    while (1)
    {
        int pixel;
        status = fscanf(pgmFile, "%d", &pixel);

        if (status == EOF)
        {
            break;
        }

        ptrPgm->pixelsI[pixelIndex] = pixel;

        if (pixelIndex % 1000 == 0)
        {
            // printf( "Pixel %d: %d \n", pixelIndex, pixel);
        }

        pixelIndex++;
    }

    ConvertP2ToP5(ptrPgm);

    //6. Write	the data buffer to the file using a binary format (either P5 or P6)

}

void ProcessP5(FILE *pgmFile, pgmStruct* ptrPgm)
{

}

void ProcessP6(FILE *pgmFile, pgmStruct* ptrPgm)
{

}


void ConvertP2ToP5(pgmStruct* ptrPgm)
{
    // initialize new pgm
    pgmStruct newPgm;
    newPgm.pixels = NULL;
    newPgm.pixelsB = NULL;
    newPgm.pixelsI = NULL;
    // loop through pixels of ptrPgm
    int i;
    i = ptrPgm->width * ptrPgm->height;
    for(i=i; i>0; i--) {
        // read int as binary to new pgm file

    }
    // set &ptrPgm to new pgm
}

void SaveP5(FILE *output, pgmStruct* ptrPgm)
{

}

void SaveP6(FILE *output, pgmStruct* ptrPgm)
{

}
