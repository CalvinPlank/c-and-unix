#ifndef PGM
#define PGM

// https://www.programiz.com/c-programming/c-file-input-output

#include <stdlib.h>
#include <stdio.h>

typedef struct BinaryInfo {
	int size;
	size_t bytes[128];
} BinaryInfo;

typedef struct Pixel {
    BinaryInfo r, g, b;
} Pixel;

typedef struct PGM {
	int width;
	int height;
	int intensity;
	Pixel* pixels;
	BinaryInfo* pixelsB;
	int* pixelsI;
} pgmStruct;

//pgmStruct readDataFromTag(char tag[2]);

//binaryInfo writeDataFromStruct(pgmStruct pgmStruct);

void ProcessP2(FILE *pgmFile, pgmStruct* ptrPgm);
void ProcessP5(FILE *pgmFile, pgmStruct* ptrPgm);
void ProcessP6(FILE *pgmFile, pgmStruct* ptrPgm);

void ConvertP2ToP5(pgmStruct* ptrPgm);

void SaveP5(FILE *output, pgmStruct* ptrPgm);
void SaveP6(FILE *output, pgmStruct* ptrPgm);

#endif
