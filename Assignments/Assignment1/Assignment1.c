#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 8

typedef struct _Matrix {
    double element[N][N];
} Matrix;


void PrintMatrix(Matrix a);
float ComputeAverage(Matrix a);
Matrix Add(Matrix a, int dc);
Matrix MatrixTranspose(Matrix a);
Matrix MatrixElementDiv(Matrix a, Matrix b);
Matrix MatrixElementMult(Matrix a, Matrix b);
int Round(double a);

Matrix Q50 = {{16, 11, 10, 16, 24, 40, 51, 61,
    12, 12, 14, 19, 26, 58, 60, 55,
    14, 13, 16, 24, 40, 57, 69, 56,
    14, 17, 22, 29, 51, 87, 80, 62,
    18, 22, 37, 56, 68,109,103, 77,
    24, 35, 55, 64, 81,104,113, 92,
    49, 64, 78, 87,103,121,120,101,
    72, 92, 95, 98,112,100,103, 99}
};

int main(int argc, const char * argv[])
{
    Matrix M = {{154, 123, 123, 123, 123, 123, 123, 136,
        192, 180, 136, 154, 154, 154, 136, 110,
        254, 198, 154, 154, 180, 154, 123, 123,
        239, 180, 136, 180, 180, 166, 123, 123,
        180, 154, 136, 167, 166, 149, 136, 136,
        128, 136, 123, 136, 154, 180, 198, 154,
        123, 105, 110, 149, 136, 136, 180, 166,
        110, 136, 123, 123, 123, 136, 154, 136}};
    
    
    // need to implement PrintMatrix
    PrintMatrix(M);
    // need to implement ComputeAverage
    float ave = ComputeAverage(M);
    // need to implement round
    int dc = Round(ave);
    printf("Ave = %d\n\n\n",dc);
    
    // need to implement Add
    Matrix M2 = Add(M, -dc);
    // need to implement PrintMatrix
    PrintMatrix(M2);
    
    // need to implement MatrixTranspose
    Matrix T2 = MatrixTranspose(M);
    PrintMatrix(T2);
    
    // need to implement MatrixElementMult
    Matrix R = MatrixElementMult(Q50, M);
    PrintMatrix(R);
    
    // need to implement MatrixElementDiv
    Matrix C = MatrixElementDiv(R, Q50);
    PrintMatrix(C);
    
    
        
    return EXIT_SUCCESS;
}

void PrintMatrix(Matrix a) {
	int i;
	int j;
	for(i = 0; i < N; i = i + 1) {
		for(j = 0; j < N; j = j + 1) {
			int val;
			val = a.element[i][j];
			printf("%d, ", val);
		}
	printf("\n");
	}
	printf("\n\n");
}

float ComputeAverage(Matrix a) {
	int i;
	int j;
	float avg = 0;
	int divisor = N * N;
	for(i = 0; i < N; i = i + 1) {
		for(j = 0; j < N; j = j + 1) {
			avg = avg + a.element[i][j];
		}
	}
	avg = avg / divisor;
	
	return avg;
}

Matrix Add(Matrix a, int dc) {
	int i;
	int j;
	Matrix b;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			int new = a.element[i][j] + dc;
			b.element[i][j] = new;
		}
	}
	return b;
}

Matrix MatrixTranspose(Matrix a) {
	int i;
	int j;
	Matrix b;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			b.element[j][i] = a.element[i][j];
		}
	}

	return b;
}

Matrix MatrixElementDiv(Matrix a, Matrix b) {
	int i;
	int j;
	double result;
	Matrix divd;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			result = a.element[i][j] / b.element[i][j];
			divd.element[i][j] = Round(result);
		}
	}

	return divd;
}

Matrix MatrixElementMult(Matrix a, Matrix b) {
	int i;
	int j;
	Matrix mltd;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			mltd.element[i][j] = b.element[i][j] * a.element[i][j];
		}
	}

	return mltd;
}

int Round(double a) {
	int b = (int) (a + 0.1);
	return b;
}
