#include <stdio.h>
#include <stdlib.h>

#include "matrices.h"
#include "fileio.h"

#define N 8

typedef struct pos {
    int row, col;
} Position;

typedef struct occurrenceAmount {
	int val, amount;
} OccurrenceAmount;

typedef struct occurrenceList {
	int size;
	OccurrenceAmount amounts[N*N];
} OccurrenceList;

Position ZigZag[N*N] = {
    {0,0},
    {0,1},{1,0},
    {2,0},{1,1},{0,2},
    {0,3},{1,2},{2,1},{3,0},
    {4,0},{3,1},{2,2},{1,3},{0,4},
    {0,5},{1,4},{2,3},{3,2},{4,1},{5,0},
    {6,0},{5,1},{4,2},{3,3},{2,4},{1,5},{0,6},
    {0,7},{1,6},{2,5},{3,4},{4,3},{5,2},{6,1},{7,0},
    {7,1},{6,2},{5,3},{4,4},{4,5},{2,6},{1,7},
    {2,7},{3,6},{4,5},{5,4},{6,3},{7,2},
    {7,3},{6,4},{5,5},{4,6},{3,7},
    {4,7},{5,6},{6,5},{7,4},
    {7,5},{6,6},{5,7},
    {6,7},{7,6},
    {7,7}
};

int data[N*N];

void PrintArray(int* a);
void PrintOccurrences(OccurrenceList a);
int* ZigZagOrdering(Matrix x);  // fill data and return data
OccurrenceList ConsecutiveOccurrance(int *x);
//Matrix InverseZigZagOrdering(int *x);  // pass in data and return a matrix

void PrintArray(int* arr) {
	int i;
	
	for (i = 0; i < N*N; i++) {
		printf("%d ", arr[i]);
	}
	
	printf("\n");
}

void PrintOccurrences(OccurrenceList occurs) {
	int i;
	
	for (i = 0; i < occurs.size; i++) {
		printf("%d %d \n", occurs.amounts[i].val, occurs.amounts[i].amount);
	}
	
	printf("\n");
}

int* ZigZagOrdering(Matrix x) {
	int i;
	
	for (i = 0; i < 64; i++) {
		Position ziggy = ZigZag[i];
		data[i] = x.element[ziggy.row][ziggy.col];
	}
	
	return data;
}

OccurrenceList ConsecutiveOccurrance(int* arr) {
	OccurrenceList occurs;
	int latest = 0;
	int index;
	
	occurs.amounts[latest].val = arr[0];
	occurs.amounts[latest].amount = 1;
	
	for (index = 1; index < 64; index++) {
		if (occurs.amounts[latest].val == arr[index]) {
			occurs.amounts[latest].amount += 1;
		} else {
			latest += 1;
			occurs.amounts[latest].val = arr[index];
			occurs.amounts[latest].amount = 1;
		}
	}
	
	occurs.size = latest + 1;
	return occurs;
};

EncodedInfo convertToBytes(OccurrenceList occurrences) {
	EncodedInfo info;
	int i;
	
	for (i = 0; i < occurrences.size; i++) {
		int byte1 = 0;
		int byte2 = occurrences.amounts[i].val;
		
		byte1 = occurrences.amounts[i].amount;
		byte1 = byte1 > 127 ? 127 : byte1;
		byte1 = byte1 < 0 ? 0 : byte1;
		
		if (byte2 < 0) {
			byte1 += 128;
			byte2 = -byte2;
		}
		
		info.bytes[2*i] = (byte) byte1;
		info.bytes[2*i + 1] = (byte) byte2;
	}
	
	info.size = occurrences.size * 2;
	
	return info;
}

OccurrenceList convertFromBytes(EncodedInfo info) {
	OccurrenceList occurrences;
	int i;
	
	for (i = 0; i < info.size / 2; i++) {
		int amount = (int) info.bytes[2*i];
		int val = (int) info.bytes[2*i + 1];
		
		if (amount > 127) {
			amount -= 128;
			val = -val;
		}
		
		occurrences.amounts[i].amount = amount;
		occurrences.amounts[i].val = val;
	}
	
	occurrences.size = info.size / 2;
	
	return occurrences;
}

void decodeOccurrences(OccurrenceList occurrences, int* decodedData) {
	int index = 0;
	int i;
	
	for (i = 0; i < occurrences.size; i++) {
		int j;
		
		for (j = 0; j < occurrences.amounts[i].amount; j++) {
			decodedData[index++] = occurrences.amounts[i].val;
		}
	}
}

Matrix dataToZigZag(int* data) {
	Matrix a;
	int i;
	
	for (i = 0; i < N*N; i++) {
		Position ziggy = ZigZag[i];
		a.element[ziggy.row][ziggy.col] = data[i];
	}
	
	return a;
}

void print(double a, int x, int y) {
	printf("%.0f ", a);
	
	if (x == (N - 1)) {
		printf("\n");
	}
	
	if (y == (N - 1) && x == (N - 1)) {
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		printf("Provide a filename to store the data in");
		return 1;
	}
	
    // special case that allows us to initialize this way
    Matrix C = {
		10, 4, 2,  5,  1, 0, 0, 0,
		3,  9, 1,  2,  1, 0, 0, 0,
	   -7, -5, 1, -2, -1, 0, 0, 0,
	   -3, -5, 0, -1,  0, 0, 0, 0,
	   -2,  1, 0,  0,  0, 0, 0, 0,
		0,  0, 0,  0,  0, 0, 0, 0,
		0,  0, 0,  0,  0, 0, 0, 0,
		0,  0, 0,  0,  0, 0, 0, 0
	};
  
    // Print out the C in zig zag order
    PrintArray(ZigZagOrdering(C));
    
    // Count the consecutive occurrence
	OccurrenceList occurrences = ConsecutiveOccurrance(data);
	
    // Print out the value and its consecutive occurrence
	PrintOccurrences(occurrences);
	
	// Turn occurrence data into binary info
	EncodedInfo info = convertToBytes(occurrences);
	
	//*
	// Write it to binary file
	writeToFile(argv[1], info);
	
	printf("\n\n~*~ WROTE TO FILE ~*~\n\n");
	
	// Get info back from file
	EncodedInfo returnedInfo;
	readFromFile(argv[1], &returnedInfo);
	//*/
	
	OccurrenceList returnedOccurrences = convertFromBytes(info);
	
	PrintOccurrences(returnedOccurrences);
	
	int returnedData[N*N];
	decodeOccurrences(returnedOccurrences, returnedData);
	
	Matrix C2 = dataToZigZag(returnedData);
	
	MatrixForEachExt(C2, &print);
}

