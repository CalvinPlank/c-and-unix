#ifndef MATRICES
#define MATRICES

typedef struct matrix {
	double element[8][8];
} Matrix;

typedef void (*forEachFunction)(double a);
typedef void (*forEachExtFunction)(double a, int b, int c);

typedef double (*mapFunction)(double a);
typedef double (*mapExtFunction)(double a, int b, int c);

typedef double (*elementMapFunction)(double a, double b);
typedef double (*elementMapExtFunction)(double a, double b, int c, int d);

typedef double (*zipFunction)(double a, double b);
typedef double (*reduceFunction)(double a, double b);

void MatrixForEach(Matrix m, forEachFunction func);
void MatrixForEachExt(Matrix m, forEachExtFunction func);

Matrix MatrixMap(Matrix m, mapFunction func);
Matrix MatrixMapExt(Matrix m, mapExtFunction func);

Matrix MatrixElementMap(Matrix m, double val, elementMapFunction func);
Matrix MatrixElementMapExt(Matrix m, double val, elementMapExtFunction func);

Matrix MatrixZip(Matrix m1, Matrix m2, zipFunction func);
double MatrixReduce(Matrix m, double init, reduceFunction func);

Matrix MatrixTranspose(Matrix m);

#endif
