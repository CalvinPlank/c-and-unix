#include <stdio.h>
#include "fileio.h"

int writeToFile(char* filename, EncodedInfo data) {
	FILE* writeFile = fopen(filename, "wb");
	
	if (writeFile == NULL) {
		printf("Could not write to file: %s", filename);
		return 1;
	}

	fwrite(data.bytes, sizeof(byte), data.size, writeFile);
	fclose(writeFile);
	
	return 0;
}

int readFromFile(char* filename, EncodedInfo* returnData) {
	FILE* readFile = fopen(filename, "rb");
	
	if (readFile == NULL) {
		printf("Could not read from file: %s", filename);
		return 1;
	}
	
	fseek(readFile, 0, SEEK_END);
	int fileSize = ftell(readFile);
	rewind(readFile);
	
	(*returnData).size = fileSize;
	
	int readSize = fread((*returnData).bytes, 1, fileSize, readFile);
	
	if (readSize != fileSize) {
		printf("Error reading from file: %s", filename);
		return 1;
	}
	
	return 0;
}
