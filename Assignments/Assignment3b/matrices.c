#include "matrices.h"
#include <stdio.h>

#define N 8

void MatrixForEach(Matrix m, forEachFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			func(m.element[y][x]);
		}
	}
}

void MatrixForEachExt(Matrix m, forEachExtFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			func(m.element[y][x], x, y);
		}
	}
}

Matrix MatrixMap(Matrix m, mapFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			m.element[y][x] = func(m.element[y][x]);
		}
	}
	
	return m;
}

Matrix MatrixMapExt(Matrix m, mapExtFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			m.element[y][x] = func(m.element[y][x], x, y);
		}
	}
	
	return m;
}

Matrix MatrixElementMap(Matrix m, double val, elementMapFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			m.element[y][x] = func(m.element[y][x], val);
		}
	}
	
	return m;
}

Matrix MatrixElementMapExt(Matrix m, double val, elementMapExtFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			m.element[y][x] = func(m.element[y][x], val, x, y);
		}
	}
	
	return m;
}

Matrix MatrixZip(Matrix m1, Matrix m2, zipFunction func) {
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			m1.element[y][x] = func(m1.element[y][x], m2.element[y][x]);
		}
	}
	
	return m1;
}

double MatrixReduce(Matrix m, double init, reduceFunction func){
	int y, x;
	
	for (y = 0; y < N; y++) {
		for (x = 0; x < N; x++) {
			init = func(m.element[y][x], init);
		}
	}
	
	return init;
}

Matrix MatrixTranspose(Matrix m) {
	int i, j;
	Matrix m2 = m;
	
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			m2.element[j][i] = m.element[i][j];
		}
	}
	
	return m2;
}
