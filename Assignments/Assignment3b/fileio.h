#ifndef FILEIO
#define FILEIO

typedef unsigned char byte;

typedef struct encodedInfo {
	int size;
	byte bytes[128];
} EncodedInfo;

int writeToFile(char* filename, EncodedInfo b);
int readFromFile(char* filename, EncodedInfo* b);

#endif
