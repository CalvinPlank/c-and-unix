#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 8

typedef struct _Matrix {
    double element[N][N];
} Matrix;


void PrintMatrix(Matrix a);
void Init(int quality);
float ComputeAverage(Matrix a);
Matrix Add(Matrix a, int dc);
Matrix MatrixTranspose(Matrix a);
Matrix MatrixElementDiv(Matrix a, Matrix b);
Matrix MatrixElementMult(Matrix a, Matrix b);
int Round(double a);

Matrix Q50 = {{16, 11, 10, 16, 24, 40, 51, 61,
    12, 12, 14, 19, 26, 58, 60, 55,
    14, 13, 16, 24, 40, 57, 69, 56,
    14, 17, 22, 29, 51, 87, 80, 62,
    18, 22, 37, 56, 68,109,103, 77,
    24, 35, 55, 64, 81,104,113, 92,
    49, 64, 78, 87,103,121,120,101,
    72, 92, 95, 98,112,100,103, 99}
};

Matrix T;
Matrix Q;

int main(int argc, const char **argv)
{	
	int qual = atoi(argv[1]);

	printf("quality = %u\n\n", qual);

	Init(qual);

	//PrintMatrix(T);
	PrintMatrix(Q);

    Matrix M = {{154, 123, 123, 123, 123, 123, 123, 136,
        192, 180, 136, 154, 154, 154, 136, 110,
        254, 198, 154, 154, 180, 154, 123, 123,
        239, 180, 136, 180, 180, 166, 123, 123,
        180, 154, 136, 167, 166, 149, 136, 136,
        128, 136, 123, 136, 154, 180, 198, 154,
        123, 105, 110, 149, 136, 136, 180, 166,
        110, 136, 123, 123, 123, 136, 154, 136}};
    
    
    // need to implement PrintMatrix
    //PrintMatrix(M);
    // need to implement ComputeAverage
    float ave = ComputeAverage(M);
    // need to implement round
    int dc = Round(ave);
    printf("Average of M = %d\n\n\n",dc);
    
    // need to implement Add
    M = Add(M, -dc);
    // need to implement PrintMatrix
    //PrintMatrix(M);
    
    // need to implement MatrixTranspose
    Matrix T2 = MatrixTranspose(T);
    //PrintMatrix(T2);
    
    // need to implement MatrixElementMult
    Matrix d = MatrixElementMult(T, M);
	Matrix D = MatrixElementMult(d, T2);
    //PrintMatrix(D);
    
    // need to implement MatrixElementDiv
    Matrix C = MatrixElementDiv(D, Q);

	Matrix M2 = MatrixElementMult(T2, T);

    PrintMatrix(C);
    
	PrintMatrix(M2);    

    return EXIT_SUCCESS;
}

void PrintMatrix(Matrix a) {
	int i;
	int j;
	for(i = 0; i < N; i = i + 1) {
		for(j = 0; j < N; j = j + 1) {
			int val;
			val = a.element[i][j];
			printf("%d, ", val);
		}
	printf("\n");
	}
	printf("\n\n");
}

float ComputeAverage(Matrix a) {
	int i;
	int j;
	float avg = 0;
	int divisor = N * N;
	for(i = 0; i < N; i = i + 1) {
		for(j = 0; j < N; j = j + 1) {
			avg = avg + a.element[i][j];
		}
	}
	avg = avg / divisor;
	
	return avg;
}

Matrix Add(Matrix a, int dc) {
	int i;
	int j;
	Matrix b;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			int new = a.element[i][j] + dc;
			b.element[i][j] = new;
		}
	}
	return b;
}

Matrix MatrixTranspose(Matrix a) {
	int i;
	int j;
	Matrix b;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			b.element[j][i] = a.element[i][j];
		}
	}

	return b;
}

Matrix MatrixElementDiv(Matrix a, Matrix b) {
	int i;
	int j;
	double result;
	Matrix divd;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			result = a.element[i][j] / b.element[i][j];
			divd.element[i][j] = Round(result);
		}
	}

	return divd;
}

Matrix MatrixElementMult(Matrix a, Matrix b) {
	int i;
	int j;
	Matrix mltd;
	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			mltd.element[i][j] = b.element[i][j] * a.element[i][j];
		}
	}

	return mltd;
}

int Round(double a) {
	int b = (int) (a + 0.1);
	return b;
}

void Init(int quality) {

	double r1 = (1/50) * (100 - quality);
	double r2 = 50/quality;
	double scale = (quality > 50 ? r1 : r2);

	int i;
	int j;
	for(j=0;j<N;j=j+1){
		T.element[0][j] = Round(1/sqrt(N));
	}

	for(i=1;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			T.element[i][j] = Round(sqrt(2/N)*cos(((2*j)*i*M_PI)/(2*N)));
		}
	}

	for(i=0;i<N;i=i+1){
		for(j=0;j<N;j=j+1){
			Q.element[i][j] = Q50.element[i][j]*scale;
			if(Q.element[i][j] < 1) {
				Q.element[i][j] = 1;
			} else if(Q.element[i][j] > 255) {
				Q.element[i][j] = 255;
			}
		}
	}

}
