#include <stdio.h>
#include <stdlib.h>

#define N 8

typedef struct _Matrix {
    double element[N][N];
} Matrix;

typedef struct pos {
    int row, col;
} Position;

typedef struct occurenceAmount {
	int val, amount;
} OccurenceAmount;

typedef struct occurenceList {
	int size;
	OccurenceAmount amounts[N*N];
} OccurenceList;

Position ZigZag[N*N] = {
    {0,0},
    {0,1},{1,0},
    {2,0},{1,1},{0,2},
    {0,3},{1,2},{2,1},{3,0},
    {4,0},{3,1},{2,2},{1,3},{0,4},
    {0,5},{1,4},{2,3},{3,2},{4,1},{5,0},
    {6,0},{5,1},{4,2},{3,3},{2,4},{1,5},{0,6},
    {0,7},{1,6},{2,5},{3,4},{4,3},{5,2},{6,1},{7,0},
    {7,1},{6,2},{5,3},{4,4},{4,5},{2,6},{1,7},
    {2,7},{3,6},{4,5},{5,4},{6,3},{7,2},
    {7,3},{6,4},{5,5},{4,6},{3,7},
    {4,7},{5,6},{6,5},{7,4},
    {7,5},{6,6},{5,7},
    {6,7},{7,6},
    {7,7}
};

int data[N*N];

int* ZigZagOrdering(Matrix x);  // fill data and return data
Matrix InverseZigZagOrdering(int *x);  // pass in data and return a matrix
void EncodeRunlength(const char *dataFile, Matrix C);
void DecodeRunlength(const char *dataFile);
void PrintArray (int* x);
void PrintOccurences(OccurenceList o);
OccurenceList CountOccurences(int* x);

int main(int argc, char **argv)
{
    // special case that allows us to initialize this way
    Matrix C =    {10,  4, 2,  5, 1, 0, 0, 0,
                    3,  9, 1,  2, 1, 0, 0, 0,
                   -7, -5, 1, -2, -1, 0, 0, 0,
                   -3, -5, 0, -1, 0, 0, 0, 0,
                   -2,  1, 0,  0, 0, 0, 0, 0,
                    0,  0, 0,  0, 0, 0, 0, 0,
                    0,  0, 0,  0, 0, 0, 0, 0,
                    0,  0, 0,  0, 0, 0, 0, 0};
  
    // Print out the C in zig zag order
	int* zigZagOrdered = ZigZagOrdering(C);
    PrintArray(zigZagOrdered);
    // Count the consecutive occurrence
	PrintOccurences(CountOccurences(zigZagOrdered));
    // Print out the value and its consecutive occurrence
    
    
    
    // encoding
    //EncodeRunlength("output.dat", C);
    
    //decoding
    //DecodeRunlength("output.dat");

    void PrintMatrix(Matrix a) {
	int i;
	int j;
	for(i = 0; i < N; i = i + 1) {
		for(j = 0; j < N; j = j + 1) {
			int val;
			val = a.element[i][j];
			printf("%d, ", val);
		}
	printf("\n");
	}
	printf("\n\n");
    }

}

int* ZigZagOrdering(Matrix x) {
	int i;
	for(i=0; i<N*N; i=i+1) {
		Position ziggy = ZigZag[i];
		data[i] = x.element[ziggy.row][ziggy.col];
	}

	return data;
}

void PrintArray (int* x) {
	int i;
	for(i=0; i<N*N; i=i+1) {
		printf("%d ", x[i]);
	}
	printf("\n\n");
}

void PrintOccurences(OccurenceList o) {
	int i;
	for(i=0;i<o.size;i=i+1) {
		printf("%d, %d, \n", o.amounts[i].val, o.amounts[i].amount);
	}
}

OccurenceList CountOccurences(int* x){
	OccurenceList list;
	int latest = 0;
	int i;

	list.amounts[latest].val = x[0];
	list.amounts[latest].amount = 1;

	for (i=1;i<N*N;i=i+1) {
		if(list.amounts[latest].val == x[i]) {
			list.amounts[latest].amount++;
		} else {
			latest = latest + 1;
			list.amounts[latest].val = x[i];
			list.amounts[latest].amount = 1;
		}
	}

	list.size = latest+1;
	return list;
}

